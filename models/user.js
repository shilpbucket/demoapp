var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
  timeslot: String,
  name: String,
  phone: String,
  created_date: {
    type: Date,
    default: Date.now
  },
  updated_date: {
    type: Date,
    default: Date.now
  },

});

module.exports = mongoose.model('User', UserSchema);
