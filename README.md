# Demoapp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

 ## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## How To Run

To run Angular 5 app in your system you must have @angular/cli package install globally in nodejs eco system
To Install and run any angular project refer
(https://github.com/angular/angular-cli/wiki)

After that do run npm start command to start server
open URL that listed below you will see angular production build is now ready test application 
(http://localhost:3000)

## Heroku Git
Heroku URL
(https://quiet-bayou-73807.herokuapp.com/)

Git URL
(https://git.heroku.com/quiet-bayou-73807.git)

