import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from "rxjs/Observable";
import { CommonService } from './../services/common.service';

@Injectable()
export class AuthGuardService implements CanActivate {



  constructor(private router: Router, private cS: CommonService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
  
    if (this.cS.timeslot) {

      return true;
    }
    this.router.navigate(['/']);
    return false;
  }

}
