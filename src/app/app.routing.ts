import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { AddUserComponent } from './components/add-user/add-user.component';
import { PreBookUserComponent } from './components/pre-book-user/pre-book-user.component';
import { MainScreenComponent } from './components/main-screen/main-screen.component';
import { AuthGuardHomeService } from './guard/auth-guard-home.service';
import { AuthGuardService } from './guard/auth-guard.service';

const routes: Routes = [{
    path: 'book',
    canActivate: [AuthGuardService],
    component: AddUserComponent,
    data: { title: 'Book Appointment' }
},
{
    path: 'pre-book',
    component: PreBookUserComponent,
    data: { title: 'Book TimeSlot' }

}, {
    path: 'home',
    canActivate: [AuthGuardHomeService],
    component: MainScreenComponent,
    data: { title: 'Book TimeSlot' }

}, {
    path: '',
    redirectTo: '/',
    pathMatch: 'full'
},
{
    path: '**', redirectTo: '/'
}];

export const Routing: ModuleWithProviders = RouterModule.forRoot(routes);