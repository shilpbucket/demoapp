import { Component, OnDestroy } from '@angular/core';
import { CommonService } from "./services/common.service";
import { Router } from "@angular/router";
import { Subscription } from 'rxjs/Subscription';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {


  title = 'app';

  timeSlotModal: string;
  timeSlotArr: Array<string>;
  toggleShow: boolean;
  name: string;
  phone: string;
  msg: string;
  isDisabled: boolean;
  isRed: boolean;


  timeSlotSubscription: Subscription;

  constructor(private router: Router,
    private cS: CommonService) {
    this.timeSlotArr = this.getTimeSlot();
    this.isDisabled = false;
    this.isRed = false;
    this.toggleShow = true;

    this.timeSlotSubscription = this.cS.getMessage().subscribe(data => {
      // console.log(data);
      if (data.key === 'disabled') {
        this.isDisabled = data.val;
      }
    });

  }

  ngOnInit() {
    const val = this.cS.timeslot;
    this.msg = 'Please Select TimeSlot';
    val ? this.timeSlotModal = val : this.timeSlotModal = '9 AM';
  }

  ngOnDestroy(): void {
    // prevent memory lecakage
    this.timeSlotSubscription.unsubscribe();
  }

  getTimeSlot(): Array<string> {
    return [
      '9 AM',
      '10 AM',
      '11 AM',
      '12 PM',
      '1 PM',
      '2 PM',
      '3 PM',
      '4 PM',
      '5 PM'
    ]
  }

  proceed() {

    if (this.timeSlotModal) {
      this.toggleShow = !this.toggleShow;
      this.cS.timeslot = this.timeSlotModal;
      this.router.navigate(['book']);
    }
  }

  handler() {
    console.log('click');
  }
}
