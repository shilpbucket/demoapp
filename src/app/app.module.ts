import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Routing } from './app.routing';
import { AppComponent } from './app.component';
import { AddUserComponent } from './components/add-user/add-user.component';
import { PreBookUserComponent } from './components/pre-book-user/pre-book-user.component';
import { CommonService } from './services/common.service';
import { MainScreenComponent } from './components/main-screen/main-screen.component';
import { AuthGuardService } from "./guard/auth-guard.service";
import { AuthGuardHomeService } from './guard/auth-guard-home.service';

@NgModule({
  declarations: [
    AppComponent,
    AddUserComponent,
    PreBookUserComponent,
    MainScreenComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    Routing
  ],
  providers: [CommonService, AuthGuardService, AuthGuardHomeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
