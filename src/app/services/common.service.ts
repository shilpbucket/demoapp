import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from "rxjs/Observable";
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ErrorObservable } from "rxjs/observable/ErrorObservable";

@Injectable()
export class CommonService {

  public timeslot: string;
  private timeslotSub = new Subject();
  constructor(private http: HttpClient) {
  }


  setMessage(val: any) {
    this.timeslotSub.next(val);
  }

  getMessage(): Observable<any> {

    return this.timeslotSub.asObservable();
  }

  saveData(key: string, val: string) {
    localStorage.setItem(key, val);
  }

  getData(key: string): string | null {
    const slotVal = localStorage.getItem(key);
    return slotVal ? slotVal : null;
  }

  get(url: string): Observable<any> {
    return this.http.get(url).pipe(
      catchError(this.handleError)
    );
  }

  post(url: string, data: any): Observable<any> {

    const HttpHeaderOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    return this.http.post(url, data, HttpHeaderOptions).pipe(
      catchError(this.handleError)
    );
  }

  update(url: string, data: any): Observable<any> {

    const HttpHeaderOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    return this.http.put(url, data, HttpHeaderOptions).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an ErrorObservable with a user-facing error message
    return new ErrorObservable(
      'Something bad happened; please try again later.');
  };

}
