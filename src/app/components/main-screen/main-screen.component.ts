import { Component, OnInit } from '@angular/core';
import { CommonService } from './../../services/common.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-main-screen',
  templateUrl: './main-screen.component.html',
  styleUrls: ['./main-screen.component.css']
})
export class MainScreenComponent implements OnInit {

  userData: any;

  constructor(private cS: CommonService, private route: Router) { }

  ngOnInit() {
    this.userData = JSON.parse(this.cS.getData('user'));
  }


  redirectBack() {
    this.route.navigate(['book']);
  }
}
