import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonService } from './../../services/common.service';
import { Router } from '@angular/router';
import { NgForm } from "@angular/forms";
import { environment } from './../../../environments/environment';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  @ViewChild('f') fRef: NgForm;
  name: string;
  phone: string;
  timeSlot: string;
  _userId: string;

  constructor(private cS: CommonService,
    private route: Router) {
    this.cS.setMessage({ key: 'disabled', val: true });
  }

  ngOnInit() {

    if (this.cS.getData('user')) {
      const userObj = JSON.parse(this.cS.getData('user'));
      this._userId = userObj._id;
      this.name = userObj.name;
      this.phone = userObj.phone;
      this.timeSlot = userObj.timeslot;
    } else {
      this.timeSlot = this.cS.timeslot;
    }
  }

  save() {
    if (this.fRef.valid) {
      const saveUser = {
        name: this.name,
        phone: this.phone,
        timeslot: this.cS.timeslot
      };

      if (this._userId) {

        this.cS.update(environment.URL + this._userId, saveUser).subscribe(data => {
          console.log(data);
          this.cS.saveData('user', JSON.stringify(data));
          this.route.navigate(['home']);
        });
      } else {

        this.cS.post(environment.URL, saveUser).subscribe(data => {
          console.log(data);
          this.cS.saveData('user', JSON.stringify(data));
          this.route.navigate(['home']);
        });
      }
    }

  }

  redirectBack() {
    this.route.navigate(['pre-book']);
  }
}
