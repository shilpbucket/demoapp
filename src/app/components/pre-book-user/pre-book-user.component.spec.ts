import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreBookUserComponent } from './pre-book-user.component';

describe('PreBookUserComponent', () => {
  let component: PreBookUserComponent;
  let fixture: ComponentFixture<PreBookUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreBookUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreBookUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
