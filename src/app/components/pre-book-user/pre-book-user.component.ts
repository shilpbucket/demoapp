import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { CommonService } from '../../services/common.service';
@Component({
  selector: 'app-pre-book-user',
  templateUrl: './pre-book-user.component.html',
  styleUrls: ['./pre-book-user.component.css']
})
export class PreBookUserComponent implements OnInit {



  timeSlotModal: string;
  timeSlotArr: Array<string>;
  toggleShow: boolean;
  name: string;
  phone: string;
  msg: string;

  constructor(private router: Router,
    private cS: CommonService) {
    this.timeSlotArr = this.getTimeSlot();

    this.toggleShow = true;
  }

  ngOnInit() {
    const val = this.cS.timeslot;
    this.msg = 'Please Select TimeSlot';
    val ? this.timeSlotModal = val : this.timeSlotModal = '9 AM';
  }

  getTimeSlot(): Array<string> {
    return [
      '9 AM',
      '10 AM',
      '11 AM',
      '12 PM',
      '1 PM',
      '2 PM',
      '3 PM',
      '4 PM',
      '5 PM'
    ]
  }

  proceed() {

    if (this.timeSlotModal) {
      this.toggleShow = !this.toggleShow;
      this.cS.timeslot = this.timeSlotModal;
      this.router.navigate(['book']);
    }
  }

}
